set install_path=.\dist

del .\dist\py_tool.exe
del %install_path%\py_tool.exe
pyinstaller -F window_root.py -w -n py_tool
copy .\dist\py_tool.exe %install_path%\py_tool.exe
%install_path%\py_tool.exe

chcp 65001

set /p choice=请确认是否执行压缩操作？(Y/N)
if "%choice%"=="Y" (
    echo 执行压缩操作...
    REM
    del %install_path%\py_tool.7z
    D:\java\code\code-k\python\7-Zip\7z a %install_path%\py_tool.7z %install_path%\*
) else (
    echo 取消压缩操作...
)
