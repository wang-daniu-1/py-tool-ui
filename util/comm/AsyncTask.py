import threading
import time

from util.comm.msg_util import msg


class AsyncTask:
    def __init__(self, task_thread, callback):
        self.task_thread = task_thread
        self.callback = callback

    def execute_async(self):
        # 启动任务线程
        self.task_thread.start()
        # 在另一个线程中等待任务线程执行完毕
        threading.Thread(target=self._join_and_callback).start()

    def _join_and_callback(self):
        # 等待任务线程执行完毕
        self.task_thread.join()
        # 执行回调方法
        time.sleep(1)
        print('self.callback')
        self.callback()
