import os

from util.comm.path import getPath


def msg(message):
    project_path = getPath()
    msg = os.path.join(project_path, 'msg.exe')
    print(f"{msg} * \"{message}\"")
    message = handMsg(message)
    print(f"{msg} * \"{message}\"")
    os.popen(f"{msg} * \"{message}\"")

def handMsg(message):
    if len(message) > 250:
        message = message[:120] + '...' + message[-120:-1]
    return message

# if __name__ == '__main__':
#     message = "程序执行异常：HTTPConnectionPool(host='10.3.87.222', port=80): Max retries exceeded with url: /zuul/flowstatus/comm/delallhashredis?key=sys_splitconfig (Caused by NewConnectionError('<urllib3.connection.HTTPConnection object at 0x00000297C30F29C8>: Failed to establish a new connection: [WinError 10060] 由于连接方在一段时间后没有正确答复或连接的主机没有反应，连接尝试失败。'))"
#     msg(message)