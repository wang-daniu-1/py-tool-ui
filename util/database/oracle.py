import os
import threading

import cx_Oracle

from util.comm.msg_util import msg
from util.comm.path import getPath
from util.env.java.jar_util import delayed_open_url


class MyOracle():
    def __init__(self):
        super().__init__()

    def search(self, config, sql):
        # 创建数据库连接
        oracle_home = os.environ.get('ORACLE_HOME')
        if oracle_home is None:
            msg("缺少oracle环境,请配置环境,地址：https://blog.csdn.net/kay_lew/article/details/135577303")
            url = "https://blog.csdn.net/kay_lew/article/details/135577303"
            threading.Thread(target=delayed_open_url, args=(url, 2,)).start()
            return "0"

        try:
            cx_Oracle.init_oracle_client(lib_dir=r"%s" % oracle_home)
        except:
            pass

        try:
            conn = cx_Oracle.connect('%s/%s@%s:%s/%s' % (config['user'], config['password'], config['host'], config['port'], config['service_name']))
        except cx_Oracle.DatabaseError as e:
            msg("oracle连接失败，请检查连接配置; %s" % e)
            return "0"

        # 创建游标
        cursor = conn.cursor()

        # 执行查询语句
        cursor.execute(sql)

        field_list = []
        # 获取查询结果
        for row in cursor:
            field_list.append(row)

        # 关闭游标和连接
        cursor.close()
        conn.close()

        return field_list

