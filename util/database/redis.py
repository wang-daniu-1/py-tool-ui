import os
import subprocess
import time

from util.comm.path import getPath


def clear_redis(h, p, key):
    project_path = getPath()
    redis_cli = os.path.join(project_path, 'redis-cli.exe')
    print('redis---%s' % f"{redis_cli} -h {h} -p {p} del {key}")
    time.sleep(0.05)
    os.popen(f"{redis_cli} -h {h} -p {p} del {key}")


def cmd(command):
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    output, error = process.communicate()
    if error:
        print(f"Error executing command: {error.decode()}")
        output = None
    return output.decode(), error.decode()