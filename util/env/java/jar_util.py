import os
import subprocess
import threading
import webbrowser
from time import sleep

from util.comm.msg_util import msg
from util.comm.path import getPath

def jarRun(jar_path, params, url):
    if(check_java_environment() == False):
        msg("缺少java运行环境,请先配置：https://blog.csdn.net/qq_43393098/article/details/127227706")
        url = "https://blog.csdn.net/qq_43393098/article/details/127227706"
        threading.Thread(target=delayed_open_url, args=(url, 2,)).start()
        return

    project_path = getPath()
    jar_path = os.path.join(project_path, jar_path)
    if not os.path.exists(jar_path):
        msg("jar包不存在，请下载jar包放入%s路径下" % jar_path)
        url = "https://blog.csdn.net/kay_lew/article/details/135578654"
        threading.Thread(target=delayed_open_url, args=(url, 2,)).start()
        return
    cmd = ['java', "-jar", jar_path]
    cmd.extend(params)
    try:
        if url:
            threading.Thread(target=delayed_open_url, args=(url, 5,)).start()
        subprocess.run(cmd)
    except subprocess.CalledProcessError:
        msg("java程序启动失败:%s", ' '.join(cmd))

def delayed_open_url(url, t):
    sleep(t)
    webbrowser.open(url)

def check_java_environment():
    try:
        subprocess.check_output("java -version", shell=True, stderr=subprocess.STDOUT)
        print("Java environment is available.")
        return True
    except subprocess.CalledProcessError:
        print("Java environment is not available.")
        return False