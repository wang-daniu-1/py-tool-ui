import os
import tkinter as tk
from tkinter import ttk

from kf.clear_redis import window_clear_redis
from kf.code_generate import window_code_generate
from kf.dict_enum_generate import window_dict_enum
from kf.ds_parse import window_ds_parse
from exec.exec_tool import window_exec_tool
from yw import window_yw_http
from web.web_tool import window_web_tool
from exec.exec_param_tool import window_exec_param_tool

class WindowRoot(tk.Tk):
    def __init__(self):
        super().__init__()

        # 设置窗口标题
        self.title("外挂程序-v0.0.1")
        # 设置窗口大小，例如宽度 300，高度 200
        self.geometry("500x358")

        # # 创建Tab控件
        master_tabs = ttk.Notebook(self)
        master_tabs.pack(fill=tk.BOTH, expand=1)

        tab_kf = create_tabs(master_tabs, "开发工具")
        tab_kf_redis = create_tab(tab_kf, "清缓存")
        window_clear_redis.ClsRedis().frame(tab_kf_redis)
        tab_kf_ds = create_tab(tab_kf, "ds解析")
        window_ds_parse.WinDsParse().frame(tab_kf_ds)
        tab_kf_code = create_tab(tab_kf, "代码生成")
        window_code_generate.WinCodeGenerate().frame(tab_kf_code)
        tab_kf_enum = create_tab(tab_kf, "enum工具")
        window_dict_enum.Enums().frame(tab_kf_enum)

        tab_yw = create_tabs(master_tabs, "运维工具")
        window_yw_http.WinYwHttp().frame(tab_yw)

        tab_web_tool = create_tabs(master_tabs, "web工具箱")
        window_web_tool.WinWebTool().frame(tab_web_tool)

        tab_exec_tool = create_tabs(master_tabs, "快捷启动")
        window_exec_tool.WinExecTool().frame(tab_exec_tool)

        tab_exec_param_tool = create_tabs(master_tabs, "参数启动")
        window_exec_param_tool.WinExecParamTool().frame(tab_exec_param_tool)

def create_tab(master_tabs, title):
    # 创建第一级tab t1
    tree_tab = ttk.Frame(master_tabs)
    tree_tab.pack()

    master_tabs.add(tree_tab, text=title)
    return tree_tab

def frame_tab(tree_tab, frame):
    frame.frame(tree_tab)

def create_tabs(master_tabs, title):
    # 创建第一级tab t1
    tree_tab = ttk.Frame(master_tabs)
    tree_tab.pack()

    tree_tabs = ttk.Notebook(tree_tab)
    tree_tabs.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)

    master_tabs.add(tree_tab, text=title, compound=tk.TOP)
    return tree_tabs

from util.comm.path import setExec, setCwd, printPath

if __name__ == '__main__':
    setCwd(os.getcwd())
    setExec(os.path.dirname(os.path.realpath(__file__)))
    printPath()
    window_root = WindowRoot()
    # 运行主窗体
    window_root.mainloop()


